'use strict';

angular.module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'underscore',
        

        'app.config',
        'mod.nbos',
        'mod.idn',
        'mod.app',
          'mod.m34'
])

angular.module('clientApp')
.constant('CLIENT_CONFIG',{
        CLIENT_ID: 'bac66074-fbd3-4cb9-b4c7-64b000219543',
        CLIENT_SECRET: 'webclientsecret',
        CLIENT_DOMAIN : 'http://localhost:9001'
})
